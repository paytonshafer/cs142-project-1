// Buffer.cpp

#include "Buffer.h"

using namespace std;

void Buffer::display() const
{
    int ix_stop_line_ = ix_top_line_ + window_height_;
    for (int i = ix_top_line_; i < ix_stop_line_; ++i) {
        if (i < v_lines_.size())
            cout << std::setw(6) << i+1 << "  " << v_lines_[i];
        cout << '\n';
    }
}

string Buffer::check_for_anchor(const string & line)
{
    //if an anchor is in the line, add it to the anchors vector
    int index_begin = line.find("<a");
    if(index_begin != string::npos) {
        //reconstruct string up to anchor and past anchor
        string new_line_begin(line, 0, index_begin);
        int index_end = line.find('>', index_begin);
        string new_line_end(line, index_end+1);

        //create string stream for anchor data
        string substring(line, index_begin+3, index_end-index_begin-3);
        stringstream a(substring);

        //push anchor location to 'anchors_' and then update 'anchor_count_'
        string temp;
        a >> temp; //store location
        anchors_.push_back(temp);
        ++anchor_count_;
        string anchor_text;
        a >> temp; //store text

        return new_line_begin + '<' + temp + ">[" + to_string(anchor_count_) + ']' + new_line_end;

    } else {
        return line;
    }
}

bool Buffer::open(const string & new_file_name)
{
    std::ifstream file(new_file_name);
    if (!file)
        return false;

    v_lines_.clear();
    anchors_.clear();
    anchor_count_ = 0;
    // Note: the vector is cleared only after we know the file
    // opened successfully.

    string line;
    while (getline(file, line))
        v_lines_.push_back(check_for_anchor(line));



    file_name_ = new_file_name;
    ix_top_line_ = 0;
    return true;
}
bool Buffer::go(const int &link)
{
    if(link<0 || link>anchor_count_) {
        return false;
    }
    else {
        if(!open(anchors_[link-1]))
            return false;
        else
            return true;
    }
}
